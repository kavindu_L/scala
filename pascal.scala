import scala.collection.mutable.ListBuffer

// read an int from standard input
print("Enter the exponent number: ")
val exponent_number: Int = scala.io.StdIn.readLine.toInt


def pascal(rowNum: Int): Unit ={

  var temp_listBuffer1: ListBuffer[Int] = ListBuffer(0,1,0)
  var temp_listBuffer2: ListBuffer[Int] = ListBuffer()
  var pascal_num_rows: ListBuffer[List[Int]] = ListBuffer(List(1))
  for (i <- Range(0,rowNum)){

    for (n <- Range(0,(temp_listBuffer1.length-1))){

      temp_listBuffer2+=temp_listBuffer1{n}+temp_listBuffer1{n+1}

    }

    pascal_num_rows ++= List(temp_listBuffer2.toList)
    temp_listBuffer2.insert(0,0)
    temp_listBuffer2 += 0

    temp_listBuffer1=temp_listBuffer2
    temp_listBuffer2=ListBuffer.empty[Int]
  }
  val length_of_longest_string = pascal_num_rows.toList.sortWith(_.length >_.length).head.mkString(" ").length

  pascal_num_rows.foreach{
    r =>
      var length_of_string = r.mkString(" ").length
      println(" "*((length_of_longest_string-length_of_string)/2) + r.mkString(" "))
  }
}
pascal(exponent_number)
