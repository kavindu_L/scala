/***
 remove duplicates from a list
*/

import scala.collection.mutable.ListBuffer

def removeDuplicates(list: List[(String)],duplicatesBuffer :ListBuffer[String] ): ListBuffer[(String)] ={

  if(list.length == 1){
    if (duplicatesBuffer.contains(list(0))) duplicatesBuffer
    else {
      duplicatesBuffer += list(0)
      duplicatesBuffer
    }
  }
  else{
    if(duplicatesBuffer.contains(list(0))) removeDuplicates(list.slice(1,list.length),duplicatesBuffer)
    else {
      duplicatesBuffer += list(0)
      removeDuplicates(list.slice(1,list.length),duplicatesBuffer)
    }
  }

}
