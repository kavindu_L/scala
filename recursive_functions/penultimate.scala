/**
 * gets a list and returns the penultimate element
 *
 * @param list[Any]
 * @return Any
 */

def penultimate(list: List[Any]): Any =list match {
  case h :: p :: Nil => h
  case h :: l => penultimate(l)
  case _ => "unknown"
}

val elementList = List(1,4,7,8,9,10,3)

val return_value = penultimate(elementList)
println(return_value)
