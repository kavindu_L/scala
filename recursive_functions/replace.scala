/***
  find a specific string and replace it with a given value
*/


import scala.collection.mutable.ListBuffer

val original_string = "you can replace anything"
val find_value = "replace"
val replace_value= "find and replace"
var temp_buffer:ListBuffer[String] = ListBuffer()

def findAndReplace(original_str:String,
                   find_val:String,
                   replace_val:String,
                   temp_buff:ListBuffer[String]): String ={

  if(find_val.length >= original_str.length){

    if((find_val.length == original_str.length) && (find_val == original_str)){
      temp_buff += replace_val
      temp_buff.mkString
    }
    else {
      temp_buff += original_str.substring(0,original_str.length)
      temp_buff.mkString
    }

  }
  else {

    if(original_str.substring(0,find_val.length) == find_val){
      temp_buff += replace_val
      findAndReplace(original_str.slice(find_val.length,original_str.length),find_val,replace_val,temp_buff)
    }
    else {
      temp_buff += original_str(0).toString
      findAndReplace(original_str.slice(1,original_str.length),find_val,replace_val,temp_buff)

    }
  }

}

println(findAndReplace(original_string,find_value,replace_value,temp_buffer))
