/**
 * gets a list and returns the length of the list
 *
 * @param list[Any]
 * @return Any
 */

def findLength(list: List[Any],count: Int = 0) : Any= list match {
  case Nil => count
  case h :: tail => findLength(tail,count+1)
  case _ => "unknown"

}

val elementList = List(1,4,7,8,9,10,3)

val return_value = findLength(elementList)
println(return_value)
