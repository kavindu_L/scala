/***
 take a list and retun the duplicates and their counts
*/


val original_list = List("c","a","a","a","b","b","c","d","c","d")
val sorted_list = original_list.sorted
var comparing_string:String = sorted_list(0)
var results_map = scala.collection.mutable.Map[String,Int]()

def countDuplicates(item_list: List[String],
                   comparing_string:String,
                   results_map:scala.collection.mutable.Map[String,Int],
                   item_count:Int=1 ): scala.collection.mutable.Map[String,Int] ={

  if (item_list.length ==1){
    if(comparing_string == item_list(0)){
      results_map += (item_list(0) -> item_count)
      println("results map : "+results_map)
      results_map
    }
    else {
      results_map += (comparing_string -> item_count,item_list(0) -> 1)
      println("results map : "+results_map)
      results_map
    }

  }
  else {
    if(comparing_string == item_list(0)){
      var count = item_count+1
      countDuplicates(item_list.slice(1,item_list.length),comparing_string,results_map,count)
    }
    else {
      var new_comparing_string = item_list(0)
      results_map += (comparing_string -> item_count)
      countDuplicates(item_list,new_comparing_string,results_map)
    }
  }

}

countDuplicates(sorted_list,comparing_string,results_map)
