/**
 * gets a list and returns the nth element (n>0)
 *
 * @param list[Any]
 * @return Any
 */

def nthElement(num : Int, list: List[Any]): Any ={
  if(list.length == num) list(num-1)
  else nthElement(num,list.slice(0,list.length-1))

}

val elementList = List(1,4,7,8,9,10,3)

val return_value = nthElement(elementList)
println(return_value)

/**
def nthElement[A](n: Int, ls: List[A]): A = (n, ls) match {
  case (1, h :: _   ) => h
  case (n, _ :: tail) => nthElement(n - 1, tail)
  case (_, Nil      ) => throw new NoSuchElementException
}
 */
