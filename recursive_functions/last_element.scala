/**
 * gets a list and returns the last element
 *
 * @param list[Any]
 * @return Any
 */

def lastElement(list: List[Any]): Any =list match {
  case h :: Nil => h
  case h :: l => lastElement(l)
  case _ => "unknown"
}

val elementList = List(1,4,7,8,9,10,3)

val return_value = lastElement(elementList)
println(return_value)
