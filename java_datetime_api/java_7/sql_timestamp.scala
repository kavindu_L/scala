import java.sql.Timestamp

/**sql.timestamp is the date and time class use with JDBC  which represent date and time*/

  val time: Long = System.currentTimeMillis
//  println("time "+time)
  val timestamp: Timestamp = new Timestamp(1557199803)
  println("timestamp "+timestamp)

  //    timestamp.setNanos(123456)
  val nanos: Int = timestamp.getNanos // nanos = 123456
  println("nanos "+nanos)

/*
time 1556731914502
     1557199803
timestamp 2019-05-01 23:01:54.502
nanos 502000000
*/