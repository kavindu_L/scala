import java.util.{Calendar, GregorianCalendar}

/** java.util.Calendar and GregorianCalendar */

val calendar = new GregorianCalendar
println(calendar)
//java.util.GregorianCalendar[time=1556811662045,areFieldsSet=true,areAllFieldsSet=true,lenient=true,zone=sun.util.calendar.ZoneInfo[id="Asia/Colombo",offset=19800000,dstSavings=0,useDaylight=false,transitions=9,lastRule=null],firstDayOfWeek=1,minimalDaysInFirstWeek=1,ERA=1,YEAR=2019,MONTH=4,WEEK_OF_YEAR=18,WEEK_OF_MONTH=1,DAY_OF_MONTH=2,DAY_OF_YEAR=122,DAY_OF_WEEK=5,DAY_OF_WEEK_IN_MONTH=1,AM_PM=1,HOUR=9,HOUR_OF_DAY=21,MINUTE=11,SECOND=2,MILLISECOND=45,ZONE_OFFSET=19800000,DST_OFFSET=0]


val year = calendar.get(Calendar.YEAR)
val month = calendar.get(Calendar.MONTH)
val dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH) // Jan = 0, not 1

val dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)
val weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR)
val weekOfMonth = calendar.get(Calendar.WEEK_OF_MONTH)

println("year "+year)
println("month "+month)
println("dayOfMonth " + dayOfMonth )
println("dayOfWeek " + dayOfWeek)
println("weekOfYear "+weekOfYear)
println("weekOfMonth "+ weekOfMonth )

val hour = calendar.get(Calendar.HOUR) // 12 hour clock

val hourOfDay = calendar.get(Calendar.HOUR_OF_DAY) // 24 hour clock

val minute = calendar.get(Calendar.MINUTE)
val second = calendar.get(Calendar.SECOND)
val millisecond = calendar.get(Calendar.MILLISECOND)


println("hour " + hour)
println("hourOfDay "+hourOfDay )
println("minute " +minute )
println("second "+second )
println("millisecond " + millisecond )


/*
    val calendar = new GregorianCalendar

    calendar.set(Calendar.YEAR, 2009)
    val year = calendar.get(Calendar.YEAR)
    println("year "+year)
    calendar.set(Calendar.MONTH, 11) // 11 = december
    val month = calendar.get(Calendar.MONTH)
    println("month "+month)

    calendar.set(Calendar.DAY_OF_MONTH, 24) // christmas eve
    val dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH) // Jan = 0, not 1
    println("dayOfMonth " + dayOfMonth )

 */
/*
year 2009
month 11
dayOfMonth 24
 */

/*
    val calendar = new GregorianCalendar

    //set date to last day of 2009
    calendar.set(Calendar.YEAR, 2009)
    calendar.set(Calendar.MONTH, 11) // 11 = december

    calendar.set(Calendar.DAY_OF_MONTH, 31) // new years eve

 */


//add one day
//    calendar.add(Calendar.DAY_OF_MONTH, 1)

/*
//date is now jan. 1st 2010
val year = calendar.get(Calendar.YEAR) // now 2010
val month = calendar.get(Calendar.MONTH) // now 0 (Jan = 0)
val dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH) // now 1
println(year)
println(month)
println(dayOfMonth)

 */
/*
2010
0
1
 */

/*
calendar.add(Calendar.DAY_OF_MONTH, -1)
val dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)
println(dayOfMonth) //31

 */

/*

    val calendar = new GregorianCalendar
    val timeZone = calendar.getTimeZone
    println(timeZone) //sun.util.calendar.ZoneInfo[id="Asia/Colombo",offset=19800000,dstSavings=0,useDaylight=false,transitions=9,lastRule=null]


 */