import java.sql.Date

/** sql.Date is the date class use with JDBC which represent date*/


  val time: Long = System.currentTimeMillis
  val date: Date = new Date(time)

  println(date) //2019-05-02
  //Wed May 01 22:50:05 IST 2019 <- util.Date
  // 2019-05-01 <- sql.
