import java.util.Date

/**
 * java.util.Date represent date and time
 * */


  /** converts currentTimeMillis like timestamps to more readable version */
  val date: Date = new Date(1556730765)
  println(date) //Mon Jan 19 05:55:30 IST 1970

  val time = date.getTime
  println(time + " =====> 1") //1556730765

  //Wed May 01 22:42:45 IST 2019
  //1556730765362

  val now = System.currentTimeMillis
  println(now+ " =====> 2") //1556810906439

  val date2 = new Date(now)
  println(date2+ " =====> 3") //Thu May 02 20:58:52 IST 2019


  /** compare two dates */

  val date1 = new Date
  val date3 = new Date

  val comparison = date1.compareTo(date3)
  println(comparison+ " =====> 4") //0

  val date5 = new Date
  val date4 = new Date

  val isBefore = date1.before(date4)
  val isAfter = date1.after(date4)

  println(isAfter+ " =====> 5") //false
  println(isBefore+ " =====> 6") //false

