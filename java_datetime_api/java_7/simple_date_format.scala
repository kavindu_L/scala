import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.Date

val format = new SimpleDateFormat("yyyy-MM-dd")

val dateString = format.format(new Date() )
val date = format.parse("2009-12-31")

println(dateString) //2019-05-02
println(date) //Thu Dec 31 00:00:00 IST 2009




val formatter = DateTimeFormatter.BASIC_ISO_DATE

val formattedDate = formatter.format(LocalDate.now)
System.out.println(formattedDate)//20190502

val date1=new java.sql.Date(1557199803*1000);
System.out.println(date1+" =======");

//    val startDate = new DateTime(1970, 1, 1, 0, 0, 0, 0)
//    val time = startDate.AddSeconds(1268927156)


/**
/** converting timestamp */

import java.text.SimpleDateFormat
import java.util.TimeZone
//Unix seconds
val unix_seconds = 1558024793 //1557199803 //1558024326
//convert seconds to milliseconds
val date = new Date(unix_seconds * 1000L)
// format of the date
val jdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ")
jdf.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"))  //Asia/Colombo //UTC
val java_date = jdf.format(date)
System.out.println("\n" + java_date + "\n")
 */