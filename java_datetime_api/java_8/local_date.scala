import java.time.LocalDate

val localDate = LocalDate.now
val localDate2 = LocalDate.of(2015, 12, 31)
println("localDate "+localDate)
println("localDate2 "+localDate2)


val year = localDate.getYear
val month = localDate.getMonth
val dayOfMonth = localDate.getDayOfMonth
val dayOfYear = localDate.getDayOfYear
val dayOfWeek = localDate.getDayOfWeek
println()
println("year "+year)
println("month "+month)
println("dayOfMonth "+dayOfMonth)
println("dayOfYear "+dayOfYear)
println("dayOfWeek "+dayOfWeek)


val localDate3 = LocalDate.of(2015, 12, 31)

val localDate4 = localDate.plusYears(3)
val localDate5 = localDate.minusYears(3)

println()
println("localDate3 "+localDate3)
println("localDate3.plusYears "+localDate4)
println("localDate3.minusYears "+localDate5)