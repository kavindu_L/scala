import java.time.Duration
import java.time.Instant

val first = Instant.now
println("first "+first)

for (i <- Range(0,10)) {
  print(i+",")
}

val second = Instant.now
println("\n"+"second "+second)

val duration = Duration.between(first, second)

println("duration "+duration)