import java.time.Instant

val now = Instant.now
val later = now.plusSeconds(3)
val earlier = now.minusSeconds(3)

println("earlier "+earlier)
println("now "+now)
println("later "+later)