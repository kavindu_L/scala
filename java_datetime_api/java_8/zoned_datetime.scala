import java.time.{Period, ZoneId, ZonedDateTime}

val zonedDateTime = ZonedDateTime.now

val zoneId = ZoneId.of("UTC+1")
val zonedDateTime2 = ZonedDateTime.of(2015, 11, 30, 23, 45, 59, 1234, zoneId)
println("zoneId "+zoneId)
println("zonedDateTime2 "+zonedDateTime2)

val year = ZonedDateTime.now.getYear
val month = ZonedDateTime.now().getMonth().getValue()
println()
println("ZonedDateTime.now.getYear "+year)
println("ZonedDateTime.now().getMonth().getValue() "+month)

val newZoneDateTime = zonedDateTime.plus(Period.ofDays(3))
println()
println("zonedDateTime.plus(Period.ofDays(3)) "+newZoneDateTime)

val zoneId2 = ZoneId.of("UTC+1")
val zoneId3 = ZoneId.of("Europe/Paris")
println("zoneId2 "+zoneId2)
println("zoneId3 "+zoneId3)
