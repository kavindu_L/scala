import java.time.LocalTime

val localTime = LocalTime.now
val localTime2 = LocalTime.of(21, 30, 59, 11001)
println("localTime "+localTime)
println("localTime2 "+localTime2)

val localTime3 = LocalTime.of(21, 30, 59, 11001)
println()
println("localTime3 "+localTime3)

val localTimeLater = localTime3.plusHours(3)
val localTimeEarlier = localTime3.minusHours(3)
println()
println("localTimeLater "+localTimeLater)
println("localTimeEarlier "+localTimeEarlier)