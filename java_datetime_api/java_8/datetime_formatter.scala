import java.time.{LocalDate, ZonedDateTime}
import java.time.format.DateTimeFormatter

val formatter1 = DateTimeFormatter.BASIC_ISO_DATE

val formattedDate = formatter1.format(LocalDate.now)
println("formattedDate "+formattedDate)

val formatter2 = DateTimeFormatter.BASIC_ISO_DATE

val formattedZonedDate = formatter2.format(ZonedDateTime.now)
println("formattedZonedDate = " + formattedZonedDate)