import java.time.LocalDateTime

val localDateTime = LocalDateTime.now
val localDateTime2 = LocalDateTime.of(2015, 11, 26, 13, 55, 36, 123)
println("localDateTime "+localDateTime)
println("localDateTime2 "+localDateTime2)
println()

val localDateTime_a = LocalDateTime.now
val localDateTime_b = localDateTime.plusYears(3)
val localDateTime_c= localDateTime.minusYears(3)

println("localDateTime_a "+localDateTime_a)
println("localDateTime_b "+localDateTime_b)
println("localDateTime_c "+localDateTime_c)